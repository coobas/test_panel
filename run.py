import json
import logging
import os
import signal
import sys
from datetime import datetime
from functools import partial

import bokeh.command.util
import bokeh.server.server
import panel as pn
import panel.cli
import tornado.web
from tornado.gen import coroutine
from tornado.web import RequestHandler


class StatusHandler(RequestHandler):
    @coroutine
    def get(self):
        self.set_header("Content-Type", "application/json")
        self.set_status(200)

        result = {"timestamp": datetime.utcnow().isoformat() + "Z"}

        self.write(json.dumps(result))


logging.basicConfig(level=logging.DEBUG)

applications = {'/': bokeh.command.util.build_single_handler_application("app.py")}
server = bokeh.server.server.Server(
    applications,
    start=False,
    show=False,
    port=int(os.environ.get("PORT", 5006)),
    allow_websocket_origin=["*"],
    keep_alive_milliseconds=5000,
    use_xheaders=True,
    extra_patterns=[
        (r"^/status/?$", StatusHandler),
        (r"^/(favicon\.ico)$", tornado.web.StaticFileHandler, {"path": os.getcwd()}),
        # (r"/r/(.*)", StaticFileDownloadHandler, {"path": reports_dir}),
    ],
)

server.run_until_shutdown()
